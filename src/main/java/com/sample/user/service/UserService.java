/**
 * 
 */
package com.sample.user.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.sample.user.model.User;

/**
 * @author kovuru.harika
 *
 */
public interface UserService {
	
	User save(User user);

    void delete(User user);

    User findOne(String id);

    Iterable<User> findAll();

    Page<User> findByfName(String fName,  PageRequest pageRequest);

    List<User> findByemail(String email);
}
