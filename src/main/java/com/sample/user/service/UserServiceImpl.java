package com.sample.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sample.user.model.User;
import com.sample.user.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
//    public void setUserRepository(UserRepository theUserRepository) {
//        this.userRepository = theUserRepository;
//    }

	@Override
	public User save(User user) {
		 
		return userRepository.save(user);
	}

	@Override
	public void delete(User user) {
		 userRepository.delete(user);
	}

	@Override
	public User findOne(String id) {
		return userRepository.findOne(id);
	}

	@Override
	public Iterable<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public Page<User> findByfName(String fName,  PageRequest pageRequest) {
		 return userRepository.findByfName(fName, pageRequest);
	}

	@Override
	public List<User> findByemail(String email) {
		return userRepository.findByemail(email);
	}

}
