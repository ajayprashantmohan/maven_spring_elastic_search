package com.sample.user.repository;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

import com.sample.user.model.User;

@Component
public interface UserRepository extends ElasticsearchRepository<User, String>{

	Page<User> findByfName(String fName, Pageable pageable);

    List<User> findByemail(String email);
}
