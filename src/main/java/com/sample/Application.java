package com.sample;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;

import com.sample.user.model.User;
import com.sample.user.service.UserService;

@SpringBootApplication
@ComponentScan("com.sample")
public class Application implements CommandLineRunner{
	
	@Autowired
    private ElasticsearchOperations es;

    @Autowired
    private UserService userService;


	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(Application.class, args);

	}
		 
	 @Override
	    public void run(String... args) throws Exception {

	        printElasticSearchInfo();

	        Date date= new Date();
	        userService.save(new User("1001","Maker","John","makerjohn@gmail.com",603103,date,true));
	        userService.save(new User("1002","Checker","John","checkerjohn@gmail.com",603104,date,true));
	        userService.save(new User("1003","MakerChecker","John","makercheckerjohn@gmail.com",603105,date,false));
	   

	        //fuzzey search
	        Page<User> users = userService.findByfName("Maker", new PageRequest(0, 10));

	        //List<Book> books = bookService.findByTitle("Elasticsearch Basics");

	        users.forEach(x -> System.out.println(x));


	    }

    //useful for debug, print elastic search details
    private void printElasticSearchInfo() {

        System.out.println("--ElasticSearch--");
        Client client = es.getClient();
        Map<String, String> asMap = client.settings().getAsMap();

        asMap.forEach((k, v) -> {
            System.out.println(k + " = " + v);
        });
        System.out.println("--ElasticSearch--");
    }
}